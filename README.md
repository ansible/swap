# Swap and hibernation

This role configures **swap file** (not partition) on Ubuntu 18.04 and 22.04.

Additionally, it sets up hibernation (suspend to disk).
Hibernation has also been tested to work after resizing the swap file
(make sure to re-run this role after resizing swap).

Repeated observation:
this role fails on the first run (swapfile creation task),
but then succeeds on a second run (without any other interventions).
Happened on both `rosetta` and `asks2`.


## Swap

Presently, this role cannot handle hosts with swap partitions, **only swap files**.
https://stackoverflow.com/questions/24765930/add-swap-memory-with-ansible/33071279
Future work: modify this role so it can handle swap partitions too.

For example,
```
$ swapon --show
NAME      TYPE      SIZE USED PRIO
/dev/dm-2 partition 980M   0B   -2
```

For extra points, tweak this role to only disable and recreate if the existing swap size
is different to requested `swap_size` (e.g., check using `swapon --show` or `free -m`).
Because at the moment, this role is rather aggressive and always destroys any existing swap file
irrespective of whether it's of the desired size, before creating a new swap file.

+ https://stackoverflow.com/questions/24765930/add-swap-memory-with-ansible/33071279
+ https://github.com/ansible/ansible/issues/5241#issuecomment-31438159
+ https://gist.github.com/manuelmeurer/a2c0a8c24a0bb5092250
+ https://gist.github.com/devster31/74e48cc1c8e73c637bc7
+ https://stackoverflow.com/questions/53116381/double-with-items-loop-in-ansible



## Hibernation

This role successfully enabled hibernation and wake-up on Bionic and Jammy
using a swap file.

This role checks if the kernel supports hibernation by checking that
`/sys/power/state` contains the string `disk`, and that `/sys/power/disk`
does not contain the string `disabled` before proceeding.

This is a no frills implementation. For example, it does not support locked
screen on wake (but if you need that, there's nothing stopping you from
adding it yourself).

To hibernate, say `systemctl hibernate`. This role configures the sudoers file
to allow the hibernation command to be run without `sudo`. This makes it
much easier to hibernate from a shortcut (an i3 keyboard shortcut, for example).

To wake up, press any key. This role configures wakeup to **ignore** mouse movement
(because waking up unintentionally by accidentally bumping the mouse is really
annoying).

+ https://rephlex.de/blog/2019/12/27/how-to-hibernate-and-resume-from-swap-file-in-ubuntu-20-04-using-full-disk-encryption/
+ https://askubuntu.com/questions/6769/hibernate-and-resume-from-a-swap-file
+ https://askubuntu.com/questions/1034185/ubuntu-18-04-cant-resume-after-hibernate/1038856
+ https://blog.ivansmirnov.name/how-to-set-up-hibernate-on-ubuntu-20-04/
+ https://wiki.ubuntu.com/DebuggingKernelHibernate
+ https://stackoverflow.com/questions/39795873/ansible-lineinfile-modify-a-line/63396024
+ https://stackoverflow.com/questions/55844981/ansible-insert-word-in-grub-cmdline
+ https://github.com/GSA/ansible-os-ubuntu-16/issues/21
+ https://stackoverflow.com/questions/136505/searching-for-uuids-in-text-with-regex
+ https://wiki.archlinux.org/title/Power_management/Suspend_and_hibernate
+ https://zpr5.com/how-to-enable-hibernation-on-ubuntu-when-using-a-paging-file/
+ https://git.kernel.org/pub/scm/linux/kernel/git/rafael/suspend-utils.git/tree/HOWTO


### Increase verbosity in hibernation log?

I used to to be able to hibernate/wakeup for a whole month on end (15-20 cycles)
without rebooting the computer, as evident by the systemd hibernate log.

But since the changes brought on by changing the swap size, it seems
I get no more than eight cycles before hibernation hangs and forcing me to reboot.

As a first step, to confirm that we indeed have a problem with hibernation,
I wonder if we can increase the verbosity of the hibernation log?
At present, the systemd log is rather sparse (bowdlerised excerpts):
```
root@asks2:~
# journalctl -u systemd-hibernate.service
jan 15 22:27:45 asks2 systemd[1]: Starting Hibernate...
jan 15 22:27:45 asks2 run-parts[10277]: run-parts: executing /lib/systemd/system-sleep/hdparm pre
jan 16 08:52:42 asks2 run-parts[10414]: run-parts: executing /lib/systemd/system-sleep/hdparm post
jan 16 08:52:42 asks2 run-parts[10414]: /dev/sdb:
jan 16 08:52:42 asks2 run-parts[10414]:  setting Advanced Power Management level to 0xfe (254)
jan 16 08:52:42 asks2 run-parts[10414]:  APM_level        = 254
jan 16 08:52:42 asks2 systemd[1]: Started Hibernate.
jan 17 22:34:02 asks2 systemd[1]: Starting Hibernate...
jan 17 22:34:02 asks2 run-parts[6262]: run-parts: executing /lib/systemd/system-sleep/hdparm pre
jan 18 09:04:29 asks2 run-parts[6395]: run-parts: executing /lib/systemd/system-sleep/hdparm post
jan 18 09:04:29 asks2 run-parts[6395]: /dev/sdb:
jan 18 09:04:29 asks2 run-parts[6395]:  setting Advanced Power Management level to 0xfe (254)
jan 18 09:04:29 asks2 run-parts[6395]:  APM_level        = 254
jan 26 23:36:24 asks2 systemd[1]: Starting Hibernate...
jan 26 23:36:24 asks2 run-parts[5062]: run-parts: executing /lib/systemd/system-sleep/hdparm pre
jan 27 08:45:03 asks2 run-parts[5222]: run-parts: executing /lib/systemd/system-sleep/hdparm post
jan 27 08:45:04 asks2 run-parts[5222]: /dev/sdb:
jan 27 08:45:04 asks2 run-parts[5222]:  setting Advanced Power Management level to 0xfe (254)
jan 27 08:45:04 asks2 run-parts[5222]:  APM_level        = 254
jan 27 08:45:04 asks2 systemd[1]: Started Hibernate.
jan 28 02:38:55 asks2 systemd[1]: Starting Hibernate...
jan 28 02:38:55 asks2 run-parts[8188]: run-parts: executing /lib/systemd/system-sleep/hdparm pre
jan 28 09:25:15 asks2 run-parts[8318]: run-parts: executing /lib/systemd/system-sleep/hdparm post
jan 28 09:25:15 asks2 run-parts[8318]: /dev/sdb:
jan 28 09:25:15 asks2 run-parts[8318]:  setting Advanced Power Management level to 0xfe (254)
jan 28 09:25:15 asks2 run-parts[8318]:  APM_level        = 254
jan 28 09:25:15 asks2 systemd[1]: Started Hibernate.
jan 30 00:46:59 asks2 systemd[1]: Starting Hibernate...
jan 30 00:46:59 asks2 run-parts[26866]: run-parts: executing /lib/systemd/system-sleep/hdparm pre
```

Let's introduce `loglevel` in `/etc/uswsusp.conf`.

+ https://en.opensuse.org/SDB:Suspend_to_disk
+ http://manpages.ubuntu.com/manpages/bionic/man5/uswsusp.conf.5.html


### How to fix no sound after wake-up (rare occurrence on Bionic)

Happens every fourth of fifth wake-up, unclear why. It's like the sound system gets
stuck in limbo. It can be jiggled loose by killing `pulseaudio` (which
forces it to restart, I suppose) and then starting `pavucontrol`:
```
pulseaudio --kill && pavucontrol
```
As soon as `pavucontrol` opens, sound is back to normal. You can then close the
`pavucontrol` window and continue as normal.

Very occasionally (I have only experienced this once in several months),
the above does not help, and `pavucontrol` still shows no devices except for the
dummy device.

What worked for me was to forcibly reload ALSA:
```
sudo alsa force-reload
pulseaudio --kill && pavucontrol
```
Note that if you had any playing media, you need to restart its program before
its sound is picked up again.

+ https://itsfoss.com/fix-sound-ubuntu-1304-quick-tip


### Hibernation stops working after changed swap size

After successfully changing swap size using this role (so all tasks in
`hibernation.yml` did also successfully complete), hibernation no longer works.

On issuing `systemctl hibernate`, nothing happens (except for a brief network disconnect).

The systemd log shows:
```
taha@asks2:~
$ sudo journalctl -xe
-- Subject: Unit systemd-hibernate.service has begun start-up
-- Defined-By: systemd
-- Support: http://www.ubuntu.com/support
--
-- Unit systemd-hibernate.service has begun starting up.
dec 25 03:08:16 asks2 run-parts[21116]: run-parts: executing /lib/systemd/system-sleep/hdparm pre
dec 25 03:08:16 asks2 s2disk[21126]: s2disk: Could not use the resume device (try swapon -a). Reason: No such device
dec 25 03:08:16 asks2 systemd[1]: systemd-hibernate.service: Main process exited, code=exited, status=19/n/a
dec 25 03:08:16 asks2 systemd[1]: systemd-hibernate.service: Failed with result 'exit-code'.
dec 25 03:08:16 asks2 systemd[1]: Failed to start Hibernate.
```

The error (`s2disk: Could not use the resume device. Reason: No such device`) is surprising to me,
because we have taken care to update `resume=UUID` in GRUB and initramfs, and reloaded them.

Is it perhaps necessary to reboot the computer after changing the swap size
(and thus the swap UUID) in GRUB and initramfs?
Rebooting improved nothing.

Perhaps this issue is not due to changed swap size, but due to a recent kernel update?

```
root@asks2:~
# blkid
/dev/nvme0n1p1: UUID="EF66-A385" TYPE="vfat" PARTUUID="807b8b78-e3c7-4af9-bc05-35f6aa325d0d"
/dev/nvme0n1p2: UUID="3c8da905-9a76-11e9-a9ff-7085c2b607f0" TYPE="ext4" PARTUUID="84bc2c2e-6736-4486-9742-83fffe9333e4"
/dev/nvme0n1: PTUUID="6f731e56-dada-4102-9fe6-0e76d9981436" PTTYPE="gpt"
```

Tested to run `s2disk` directly, figured out how to update initramfs with fewer warnings,
and cleaned up old kernel versions.
Back to the problem at hand, which remains unfixed.

+ https://forums.linuxmint.com/viewtopic.php?t=64512

There is a file `/etc/uswsusp.conf` (must be the one actually used in place of `/etc/suspend.conf`
mentioned elsewhere), and it contains a bunch of interesting values:
```
root@asks2:~
# ll /etc/uswsusp.conf
-rw-r--r-- 1 root root 236 okt 24 09:38 /etc/uswsusp.conf
# cat /etc/uswsusp.conf
# /etc/uswsusp.conf(5) -- Configuration file for s2disk/s2both
resume device = /dev/nvme0n1p2
compress = y
early writeout = y
image size = 15486815436
RSA key file = /etc/uswsusp.key
shutdown method = platform
resume offset = 41304064
```

Note how the file has not been modified recently (certainly not since we changed
the swap size). And just by looking at it, I can tell that the offset value is
wrong.

Fixing the offset value in this file solved the issue - hibernation works again!
So this issue was most likely caused by the changed swap size, and not any kernel upgrade.
Also, hibernation works immediately after configuring the right files (and updating GRUB and initramfs),
no reboot was necessary.

I suppose there is some tool to regenerate this config file? Sort of, there is `dpkg-reconfigure uswsusp`.
(Aside from offset, also the image size value might need to be set).

> your original `/etc/uswsusp.conf` was written by the scripts invoked by `dpkg`
> while installing `uswsusp`. To see the script, check `/var/lib/dpkg/info/uswsusp.config`.

+ https://itectec.com/unixlinux/linux-where-is-hibernation-resume-getting-the-device-node-from/
+ https://dtbaker.net/blog/ubuntu-13-04-hibernate-issue-could-not-stat-the-resume-device-file-devdm-0/
+ https://askubuntu.com/questions/1116778/how-to-set-the-resume-variable-to-override-these-issues/1116795
+ https://wiki.archlinux.org/title/Uswsusp
+ https://bbs.archlinux.org/viewtopic.php?id=154003
+ https://askubuntu.com/questions/1356137/s2disk-could-not-use-the-resume-device-try-swapon-a-reason-no-such-device
+ https://askubuntu.com/questions/671941/how-can-i-redetect-swap-partition-after-formating-to-enable-hibernation?rq=1


### update-initramfs

```
root@asks2:~
# update-initramfs -u -k $(uname -r)
update-initramfs: Generating /boot/initrd.img-5.4.0-91-generic
W: Possible missing firmware /lib/firmware/rtl_nic/rtl8125a-3.fw for module r8169
W: Possible missing firmware /lib/firmware/rtl_nic/rtl8168fp-3.fw for module r8169
cryptsetup: WARNING: failed to detect canonical device of /dev/nvme0n1p2
```
(the NIC firmware warnings and the cryptsetup warning were present before as well,
I just cut it out from the output above to reduce clutter).

Regarding the cryptsetup warning, sources on the web suggest removing the `cryptsetup`
package (since we are not using disk encryption). But attempting to remove that package
would likely break the system, as it would also remove the
[`ubuntu-server`](https://packages.ubuntu.com/bionic/ubuntu-server) package:
```
root@asks2:~
# apt remove cryptsetup
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following packages will be REMOVED:
  cryptsetup overlayroot ubuntu-server
0 upgraded, 0 newly installed, 3 to remove and 0 not upgraded.
After this operation, 519 kB disk space will be freed.
Do you want to continue? [Y/n] n
Abort.
```

[This Debian bug report](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=784881)
suggests that this warning will go away when `cryptsetup`
is updated to 2.0.3 (Ubuntu 18.04 is still on 2.0.2, so this may go away when
we upgrade to the next Ubuntu LTS). Let's leave it at that.

Regarding the **missing firmware** warnings:

+ the package `linux-firmware` was already installed (v1.173.20). So that was not the issue.
+ the fix was to manually place firmware files for `rtl8125a-3.fw` and `rtl8168fp-3.fw` in
  `/lib/firmware/rtl_nic/` and then `update-initramfs`. Added to this role.

+ https://askubuntu.com/questions/1287896/w-possible-missing-firmware-lib-firmware-rtl-nic-rtl8125a-3-fw-for-module-r816 (very relevant!)
+ https://stackoverflow.com/questions/57489589/is-there-a-way-to-prevent-the-missing-firmware-problem-showing-up-when-upgradi
+ https://askubuntu.com/a/809948/749455

This fixed (removed) the warnings about firmware when updating initramfs:
```
root@asks2:~
# update-initramfs -u -k $(uname -r)
update-initramfs: Generating /boot/initrd.img-5.4.0-91-generic
cryptsetup: WARNING: failed to detect canonical device of /dev/nvme0n1p2
```



## How to remove old kernels

```
root@asks2:~
# dpkg -l | egrep -i --color "linux-image|linux-headers|linux-modules"
ii  linux-headers-4.15.0-163                 4.15.0-163.171          all     Header files related to Linux kernel version 4.15.0
ii  linux-headers-4.15.0-163-generic         4.15.0-163.171          amd64   Linux kernel headers for version 4.15.0 on 64 bit x86 SMP
ii  linux-headers-5.4.0-90-generic           5.4.0-90.101~18.04.1    amd64   Linux kernel headers for version 5.4.0 on 64 bit x86 SMP
ii  linux-headers-5.4.0-91-generic           5.4.0-91.102~18.04.1    amd64   Linux kernel headers for version 5.4.0 on 64 bit x86 SMP
ii  linux-headers-generic                    4.15.0.163.152          amd64   Generic Linux kernel headers
ii  linux-headers-generic-hwe-18.04          5.4.0.91.102~18.04.81   amd64   Generic Linux kernel headers
rc  linux-image-4.15.0-147-generic           4.15.0-147.151          amd64   Signed kernel image generic
rc  linux-image-4.15.0-151-generic           4.15.0-151.157          amd64   Signed kernel image generic
rc  linux-image-4.15.0-154-generic           4.15.0-154.161          amd64   Signed kernel image generic
rc  linux-image-4.15.0-156-generic           4.15.0-156.163          amd64   Signed kernel image generic
rc  linux-image-4.15.0-159-generic           4.15.0-159.167          amd64   Signed kernel image generic
rc  linux-image-4.15.0-161-generic           4.15.0-161.169          amd64   Signed kernel image generic
rc  linux-image-4.15.0-162-generic           4.15.0-162.170          amd64   Signed kernel image generic
ii  linux-image-4.15.0-163-generic           4.15.0-163.171          amd64   Signed kernel image generic
rc  linux-image-5.4.0-73-generic             5.4.0-73.82~18.04.1     amd64   Signed kernel image generic
rc  linux-image-5.4.0-74-generic             5.4.0-74.83~18.04.1     amd64   Signed kernel image generic
rc  linux-image-5.4.0-77-generic             5.4.0-77.86~18.04.1     amd64   Signed kernel image generic
rc  linux-image-5.4.0-80-generic             5.4.0-80.90~18.04.1     amd64   Signed kernel image generic
rc  linux-image-5.4.0-81-generic             5.4.0-81.91~18.04.1     amd64   Signed kernel image generic
rc  linux-image-5.4.0-87-generic             5.4.0-87.98~18.04.1     amd64   Signed kernel image generic
rc  linux-image-5.4.0-89-generic             5.4.0-89.100~18.04.1    amd64   Signed kernel image generic
ii  linux-image-5.4.0-90-generic             5.4.0-90.101~18.04.1    amd64   Signed kernel image generic
ii  linux-image-5.4.0-91-generic             5.4.0-91.102~18.04.1    amd64   Signed kernel image generic
ii  linux-image-generic                      4.15.0.163.152          amd64   Generic Linux kernel image
ii  linux-image-generic-hwe-18.04            5.4.0.91.102~18.04.81   amd64   Generic Linux kernel image
rc  linux-modules-4.15.0-101-generic         4.15.0-101.102          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-106-generic         4.15.0-106.107          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-108-generic         4.15.0-108.109          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-109-generic         4.15.0-109.110          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-111-generic         4.15.0-111.112          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-112-generic         4.15.0-112.113          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-115-generic         4.15.0-115.116          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-117-generic         4.15.0-117.118          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-118-generic         4.15.0-118.119          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-121-generic         4.15.0-121.123          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-122-generic         4.15.0-122.124          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-123-generic         4.15.0-123.126          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-126-generic         4.15.0-126.129          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-128-generic         4.15.0-128.131          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-129-generic         4.15.0-129.132          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-130-generic         4.15.0-130.134          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-132-generic         4.15.0-132.136          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-134-generic         4.15.0-134.138          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-135-generic         4.15.0-135.139          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-136-generic         4.15.0-136.140          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-137-generic         4.15.0-137.141          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-139-generic         4.15.0-139.143          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-140-generic         4.15.0-140.144          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-141-generic         4.15.0-141.145          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-142-generic         4.15.0-142.146          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-143-generic         4.15.0-143.147          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-144-generic         4.15.0-144.148          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-147-generic         4.15.0-147.151          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-151-generic         4.15.0-151.157          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-154-generic         4.15.0-154.161          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-156-generic         4.15.0-156.163          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-159-generic         4.15.0-159.167          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-161-generic         4.15.0-161.169          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-162-generic         4.15.0-162.170          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
ii  linux-modules-4.15.0-163-generic         4.15.0-163.171          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-29-generic          4.15.0-29.31            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-54-generic          4.15.0-54.58            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-55-generic          4.15.0-55.60            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-58-generic          4.15.0-58.64            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-60-generic          4.15.0-60.67            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-62-generic          4.15.0-62.69            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-64-generic          4.15.0-64.73            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-65-generic          4.15.0-65.74            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-66-generic          4.15.0-66.75            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-70-generic          4.15.0-70.79            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-72-generic          4.15.0-72.81            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-74-generic          4.15.0-74.84            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-76-generic          4.15.0-76.86            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-88-generic          4.15.0-88.88            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-91-generic          4.15.0-91.92            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-96-generic          4.15.0-96.97            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-4.15.0-99-generic          4.15.0-99.100           amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-5.4.0-73-generic           5.4.0-73.82~18.04.1     amd64   Linux kernel extra modules for version 5.4.0 on 64 bit x86 SMP
rc  linux-modules-5.4.0-74-generic           5.4.0-74.83~18.04.1     amd64   Linux kernel extra modules for version 5.4.0 on 64 bit x86 SMP
rc  linux-modules-5.4.0-77-generic           5.4.0-77.86~18.04.1     amd64   Linux kernel extra modules for version 5.4.0 on 64 bit x86 SMP
rc  linux-modules-5.4.0-80-generic           5.4.0-80.90~18.04.1     amd64   Linux kernel extra modules for version 5.4.0 on 64 bit x86 SMP
rc  linux-modules-5.4.0-81-generic           5.4.0-81.91~18.04.1     amd64   Linux kernel extra modules for version 5.4.0 on 64 bit x86 SMP
rc  linux-modules-5.4.0-87-generic           5.4.0-87.98~18.04.1     amd64   Linux kernel extra modules for version 5.4.0 on 64 bit x86 SMP
rc  linux-modules-5.4.0-89-generic           5.4.0-89.100~18.04.1    amd64   Linux kernel extra modules for version 5.4.0 on 64 bit x86 SMP
ii  linux-modules-5.4.0-90-generic           5.4.0-90.101~18.04.1    amd64   Linux kernel extra modules for version 5.4.0 on 64 bit x86 SMP
ii  linux-modules-5.4.0-91-generic           5.4.0-91.102~18.04.1    amd64   Linux kernel extra modules for version 5.4.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-101-generic   4.15.0-101.102          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-106-generic   4.15.0-106.107          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-108-generic   4.15.0-108.109          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-109-generic   4.15.0-109.110          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-111-generic   4.15.0-111.112          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-112-generic   4.15.0-112.113          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-115-generic   4.15.0-115.116          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-117-generic   4.15.0-117.118          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-118-generic   4.15.0-118.119          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-121-generic   4.15.0-121.123          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-122-generic   4.15.0-122.124          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-123-generic   4.15.0-123.126          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-126-generic   4.15.0-126.129          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-128-generic   4.15.0-128.131          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-129-generic   4.15.0-129.132          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-130-generic   4.15.0-130.134          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-132-generic   4.15.0-132.136          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-134-generic   4.15.0-134.138          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-135-generic   4.15.0-135.139          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-136-generic   4.15.0-136.140          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-137-generic   4.15.0-137.141          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-139-generic   4.15.0-139.143          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-140-generic   4.15.0-140.144          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-141-generic   4.15.0-141.145          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-142-generic   4.15.0-142.146          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-143-generic   4.15.0-143.147          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-144-generic   4.15.0-144.148          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-147-generic   4.15.0-147.151          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-151-generic   4.15.0-151.157          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-154-generic   4.15.0-154.161          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-156-generic   4.15.0-156.163          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-159-generic   4.15.0-159.167          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-161-generic   4.15.0-161.169          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-162-generic   4.15.0-162.170          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
ii  linux-modules-extra-4.15.0-163-generic   4.15.0-163.171          amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-29-generic    4.15.0-29.31            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-54-generic    4.15.0-54.58            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-55-generic    4.15.0-55.60            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-58-generic    4.15.0-58.64            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-60-generic    4.15.0-60.67            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-62-generic    4.15.0-62.69            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-64-generic    4.15.0-64.73            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-65-generic    4.15.0-65.74            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-66-generic    4.15.0-66.75            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-70-generic    4.15.0-70.79            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-72-generic    4.15.0-72.81            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-74-generic    4.15.0-74.84            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-76-generic    4.15.0-76.86            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-88-generic    4.15.0-88.88            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-91-generic    4.15.0-91.92            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-96-generic    4.15.0-96.97            amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-4.15.0-99-generic    4.15.0-99.100           amd64   Linux kernel extra modules for version 4.15.0 on 64 bit x86 SMP
rc  linux-modules-extra-5.4.0-73-generic     5.4.0-73.82~18.04.1     amd64   Linux kernel extra modules for version 5.4.0 on 64 bit x86 SMP
rc  linux-modules-extra-5.4.0-74-generic     5.4.0-74.83~18.04.1     amd64   Linux kernel extra modules for version 5.4.0 on 64 bit x86 SMP
rc  linux-modules-extra-5.4.0-77-generic     5.4.0-77.86~18.04.1     amd64   Linux kernel extra modules for version 5.4.0 on 64 bit x86 SMP
rc  linux-modules-extra-5.4.0-80-generic     5.4.0-80.90~18.04.1     amd64   Linux kernel extra modules for version 5.4.0 on 64 bit x86 SMP
rc  linux-modules-extra-5.4.0-81-generic     5.4.0-81.91~18.04.1     amd64   Linux kernel extra modules for version 5.4.0 on 64 bit x86 SMP
rc  linux-modules-extra-5.4.0-87-generic     5.4.0-87.98~18.04.1     amd64   Linux kernel extra modules for version 5.4.0 on 64 bit x86 SMP
rc  linux-modules-extra-5.4.0-89-generic     5.4.0-89.100~18.04.1    amd64   Linux kernel extra modules for version 5.4.0 on 64 bit x86 SMP
ii  linux-modules-extra-5.4.0-90-generic     5.4.0-90.101~18.04.1    amd64   Linux kernel extra modules for version 5.4.0 on 64 bit x86 SMP
ii  linux-modules-extra-5.4.0-91-generic     5.4.0-91.102~18.04.1    amd64   Linux kernel extra modules for version 5.4.0 on 64 bit x86 SMP
```

Unfortunately, the clean-up needs to be done manually (I have found no reliable regexp or similar):

```
# apt purge linux-image-4.15.0-147-generic linux-image-4.15.0-151-generic linux-image-4.15.0-154-generic linux-image-4.15.0-156-generic linux-image-4.15.0-159-generic linux-image-4.15.0-161-generic linux-image-4.15.0-162-generic linux-image-4.15.0-163-generic linux-image-5.4.0-73-generic   linux-image-5.4.0-74-generic   linux-image-5.4.0-77-generic   linux-image-5.4.0-80-generic   linux-image-5.4.0-81-generic   linux-image-5.4.0-87-generic   linux-image-5.4.0-89-generic   linux-modules-extra-4.15.0-101-generic linux-modules-extra-4.15.0-106-generic linux-modules-extra-4.15.0-108-generic linux-modules-extra-4.15.0-109-generic linux-modules-extra-4.15.0-111-generic linux-modules-extra-4.15.0-112-generic linux-modules-extra-4.15.0-115-generic linux-modules-extra-4.15.0-117-generic linux-modules-extra-4.15.0-118-generic linux-modules-extra-4.15.0-121-generic linux-modules-extra-4.15.0-122-generic linux-modules-extra-4.15.0-123-generic linux-modules-extra-4.15.0-126-generic linux-modules-extra-4.15.0-128-generic linux-modules-extra-4.15.0-129-generic linux-modules-extra-4.15.0-130-generic linux-modules-extra-4.15.0-132-generic linux-modules-extra-4.15.0-134-generic linux-modules-extra-4.15.0-135-generic linux-modules-extra-4.15.0-136-generic linux-modules-extra-4.15.0-137-generic linux-modules-extra-4.15.0-139-generic linux-modules-extra-4.15.0-140-generic linux-modules-extra-4.15.0-141-generic linux-modules-extra-4.15.0-142-generic linux-modules-extra-4.15.0-143-generic linux-modules-extra-4.15.0-144-generic linux-modules-extra-4.15.0-147-generic linux-modules-extra-4.15.0-151-generic linux-modules-extra-4.15.0-154-generic linux-modules-extra-4.15.0-156-generic linux-modules-extra-4.15.0-159-generic linux-modules-extra-4.15.0-161-generic linux-modules-extra-4.15.0-162-generic linux-modules-extra-4.15.0-163-generic linux-modules-extra-4.15.0-29-generic linux-modules-extra-4.15.0-54-generic linux-modules-extra-4.15.0-55-generic linux-modules-extra-4.15.0-58-generic linux-modules-extra-4.15.0-60-generic linux-modules-extra-4.15.0-62-generic linux-modules-extra-4.15.0-64-generic linux-modules-extra-4.15.0-65-generic linux-modules-extra-4.15.0-66-generic linux-modules-extra-4.15.0-70-generic linux-modules-extra-4.15.0-72-generic linux-modules-extra-4.15.0-74-generic linux-modules-extra-4.15.0-76-generic linux-modules-extra-4.15.0-88-generic linux-modules-extra-4.15.0-91-generic linux-modules-extra-4.15.0-96-generic linux-modules-extra-4.15.0-99-generic linux-modules-extra-5.4.0-73-generic linux-modules-extra-5.4.0-74-generic linux-modules-extra-5.4.0-77-generic linux-modules-extra-5.4.0-80-generic linux-modules-extra-5.4.0-81-generic linux-modules-extra-5.4.0-87-generic linux-modules-4.15.0-101-generic linux-modules-4.15.0-106-generic  linux-modules-4.15.0-108-generic linux-modules-4.15.0-109-generic linux-modules-4.15.0-111-generic linux-modules-4.15.0-112-generic linux-modules-4.15.0-115-generic linux-modules-4.15.0-117-generic linux-modules-4.15.0-118-generic linux-modules-4.15.0-121-generic linux-modules-4.15.0-122-generic linux-modules-4.15.0-123-generic linux-modules-4.15.0-126-generic linux-modules-4.15.0-128-generic linux-modules-4.15.0-129-generic linux-modules-4.15.0-130-generic linux-modules-4.15.0-132-generic linux-modules-4.15.0-134-generic linux-modules-4.15.0-135-generic linux-modules-4.15.0-136-generic linux-modules-4.15.0-137-generic linux-modules-4.15.0-139-generic linux-modules-4.15.0-140-generic linux-modules-4.15.0-141-generic linux-modules-4.15.0-142-generic linux-modules-4.15.0-143-generic linux-modules-4.15.0-144-generic linux-modules-4.15.0-147-generic linux-modules-4.15.0-151-generic linux-modules-4.15.0-154-generic linux-modules-4.15.0-156-generic linux-modules-4.15.0-159-generic linux-modules-4.15.0-161-generic linux-modules-4.15.0-162-generic linux-modules-4.15.0-163-generic linux-modules-4.15.0-29-generic  linux-modules-4.15.0-54-generic  linux-modules-4.15.0-55-generic  linux-modules-4.15.0-58-generic  linux-modules-4.15.0-60-generic  linux-modules-4.15.0-62-generic  linux-modules-4.15.0-64-generic  linux-modules-4.15.0-65-generic  linux-modules-4.15.0-66-generic  linux-modules-4.15.0-70-generic  linux-modules-4.15.0-72-generic  linux-modules-4.15.0-74-generic  linux-modules-4.15.0-76-generic  linux-modules-4.15.0-88-generic  linux-modules-4.15.0-91-generic  linux-modules-4.15.0-96-generic  linux-modules-4.15.0-99-generic  linux-modules-5.4.0-73-generic  linux-modules-5.4.0-74-generic linux-modules-5.4.0-77-generic linux-modules-5.4.0-80-generic linux-modules-5.4.0-81-generic linux-modules-5.4.0-87-generic linux-modules-5.4.0-89-generic linux-headers-4.15.0-163 linux-headers-4.15.0-163-generic
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following packages will be REMOVED:
  linux-generic* linux-headers-4.15.0-163* linux-headers-4.15.0-163-generic* linux-headers-generic*
  linux-image-4.15.0-147-generic* linux-image-4.15.0-151-generic* linux-image-4.15.0-154-generic* linux-image-4.15.0-156-generic*
  linux-image-4.15.0-159-generic* linux-image-4.15.0-161-generic* linux-image-4.15.0-162-generic* linux-image-4.15.0-163-generic*
  linux-image-5.4.0-73-generic* linux-image-5.4.0-74-generic* linux-image-5.4.0-77-generic* linux-image-5.4.0-80-generic*
  linux-image-5.4.0-81-generic* linux-image-5.4.0-87-generic* linux-image-5.4.0-89-generic* linux-image-generic*
  linux-modules-4.15.0-101-generic* linux-modules-4.15.0-106-generic* linux-modules-4.15.0-108-generic*
  linux-modules-4.15.0-109-generic* linux-modules-4.15.0-111-generic* linux-modules-4.15.0-112-generic*
  linux-modules-4.15.0-115-generic* linux-modules-4.15.0-117-generic* linux-modules-4.15.0-118-generic*
  linux-modules-4.15.0-121-generic* linux-modules-4.15.0-122-generic* linux-modules-4.15.0-123-generic*
  linux-modules-4.15.0-126-generic* linux-modules-4.15.0-128-generic* linux-modules-4.15.0-129-generic*
  linux-modules-4.15.0-130-generic* linux-modules-4.15.0-132-generic* linux-modules-4.15.0-134-generic*
  linux-modules-4.15.0-135-generic* linux-modules-4.15.0-136-generic* linux-modules-4.15.0-137-generic*
  linux-modules-4.15.0-139-generic* linux-modules-4.15.0-140-generic* linux-modules-4.15.0-141-generic*
  linux-modules-4.15.0-142-generic* linux-modules-4.15.0-143-generic* linux-modules-4.15.0-144-generic*
  linux-modules-4.15.0-147-generic* linux-modules-4.15.0-151-generic* linux-modules-4.15.0-154-generic*
  linux-modules-4.15.0-156-generic* linux-modules-4.15.0-159-generic* linux-modules-4.15.0-161-generic*
  linux-modules-4.15.0-162-generic* linux-modules-4.15.0-163-generic* linux-modules-4.15.0-29-generic*
  linux-modules-4.15.0-54-generic* linux-modules-4.15.0-55-generic* linux-modules-4.15.0-58-generic*
  linux-modules-4.15.0-60-generic* linux-modules-4.15.0-62-generic* linux-modules-4.15.0-64-generic*
  linux-modules-4.15.0-65-generic* linux-modules-4.15.0-66-generic* linux-modules-4.15.0-70-generic*
  linux-modules-4.15.0-72-generic* linux-modules-4.15.0-74-generic* linux-modules-4.15.0-76-generic*
  linux-modules-4.15.0-88-generic* linux-modules-4.15.0-91-generic* linux-modules-4.15.0-96-generic*
  linux-modules-4.15.0-99-generic* linux-modules-5.4.0-73-generic* linux-modules-5.4.0-74-generic*
  linux-modules-5.4.0-77-generic* linux-modules-5.4.0-80-generic* linux-modules-5.4.0-81-generic* linux-modules-5.4.0-87-generic*
  linux-modules-5.4.0-89-generic* linux-modules-extra-4.15.0-101-generic* linux-modules-extra-4.15.0-106-generic*
  linux-modules-extra-4.15.0-108-generic* linux-modules-extra-4.15.0-109-generic* linux-modules-extra-4.15.0-111-generic*
  linux-modules-extra-4.15.0-112-generic* linux-modules-extra-4.15.0-115-generic* linux-modules-extra-4.15.0-117-generic*
  linux-modules-extra-4.15.0-118-generic* linux-modules-extra-4.15.0-121-generic* linux-modules-extra-4.15.0-122-generic*
  linux-modules-extra-4.15.0-123-generic* linux-modules-extra-4.15.0-126-generic* linux-modules-extra-4.15.0-128-generic*
  linux-modules-extra-4.15.0-129-generic* linux-modules-extra-4.15.0-130-generic* linux-modules-extra-4.15.0-132-generic*
  linux-modules-extra-4.15.0-134-generic* linux-modules-extra-4.15.0-135-generic* linux-modules-extra-4.15.0-136-generic*
  linux-modules-extra-4.15.0-137-generic* linux-modules-extra-4.15.0-139-generic* linux-modules-extra-4.15.0-140-generic*
  linux-modules-extra-4.15.0-141-generic* linux-modules-extra-4.15.0-142-generic* linux-modules-extra-4.15.0-143-generic*
  linux-modules-extra-4.15.0-144-generic* linux-modules-extra-4.15.0-147-generic* linux-modules-extra-4.15.0-151-generic*
  linux-modules-extra-4.15.0-154-generic* linux-modules-extra-4.15.0-156-generic* linux-modules-extra-4.15.0-159-generic*
  linux-modules-extra-4.15.0-161-generic* linux-modules-extra-4.15.0-162-generic* linux-modules-extra-4.15.0-163-generic*
  linux-modules-extra-4.15.0-29-generic* linux-modules-extra-4.15.0-54-generic* linux-modules-extra-4.15.0-55-generic*
  linux-modules-extra-4.15.0-58-generic* linux-modules-extra-4.15.0-60-generic* linux-modules-extra-4.15.0-62-generic*
  linux-modules-extra-4.15.0-64-generic* linux-modules-extra-4.15.0-65-generic* linux-modules-extra-4.15.0-66-generic*
  linux-modules-extra-4.15.0-70-generic* linux-modules-extra-4.15.0-72-generic* linux-modules-extra-4.15.0-74-generic*
  linux-modules-extra-4.15.0-76-generic* linux-modules-extra-4.15.0-88-generic* linux-modules-extra-4.15.0-91-generic*
  linux-modules-extra-4.15.0-96-generic* linux-modules-extra-4.15.0-99-generic* linux-modules-extra-5.4.0-73-generic*
  linux-modules-extra-5.4.0-74-generic* linux-modules-extra-5.4.0-77-generic* linux-modules-extra-5.4.0-80-generic*
  linux-modules-extra-5.4.0-81-generic* linux-modules-extra-5.4.0-87-generic* linux-signed-generic*
0 upgraded, 0 newly installed, 138 to remove and 0 not upgraded.
After this operation, 350 MB disk space will be freed.
Do you want to continue? [Y/n]
```

+ https://askubuntu.com/questions/1253347/how-to-easily-remove-old-kernels-in-ubuntu-20-04-lts
