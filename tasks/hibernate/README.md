# uswsusp

µswsusp (userspace software suspend) is a set of user-space tools used for hibernation
(suspend-to-disk) and suspend (suspend-to-RAM or standby) on Linux systems.
It consists of:

+ s2ram
+ s2disk
+ s2both

µswsusp was available on Ubuntu 18.04 (Focal), but is no longer available on 22.04 (Jammy),
so we can't use the nice tool it provides to find the swap offset value.
It is unclear if the functionality this package provided has been replaced b
something else or simply dropped.

On Jammy `pm-is-supported` does not output anything, neither zero or one:
```
taha@asks2:~
$ sudo pm-is-supported --suspend
taha@asks2:~
$ sudo pm-is-supported --hibernate
```

On Ubuntu Jammy, simply issuing `systemctl hibernate` (without any prior configuration)
caused the computer to shutdown, but on reboot a fresh state was returned.


+ http://blog.holdenkarau.com/2022/05/making-hibernate-work-on-ubuntu-2204.html
+ https://www.linuxuprising.com/2021/08/how-to-enable-hibernation-on-ubuntu.html
+ https://www.digitalocean.com/community/tutorials/how-to-add-swap-space-on-ubuntu-22-04
+ https://wiki.archlinux.org/title/Uswsusp
+ https://manpages.ubuntu.com/manpages/jammy/man8/pm-action.8.html
+ https://manpages.ubuntu.com/manpages/jammy/man1/pm-is-supported.1.html
