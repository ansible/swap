---

- name: Configure swap file
  when: swap_file_configure | bool | default(false)
  block:

    - name: Check if any swap file exists
      ansible.builtin.stat:
        path: "{{ item }}"
      loop: "{{ swap_file }}"
      register: swap_file_check

    - name: Check if any swap file is enabled
      # grep -w because I don't want /swap matching /swapfile, for example
      ansible.builtin.shell: "swapon --show | grep -w {{ item.item }}"
      register: swap_enabled
      loop: "{{ swap_file_check.results }}"
      changed_when: false
      failed_when: false

    # One way to make these tasks less destructive (right now swap file is destroyed irregardless)
    # could be to put suitable tasks inside an Ansible block only executed if the wanted
    # swap_file is already enabled (or some such)
    # But I guess that is not good enough, because even if the desired swap_file exists,
    # nothing says other swap files can't be polluting the system...
    # More thinking required...

    # The logic here is simple: look for enabled swap files, and disable them.
    # The reason we cannot simply disable *all* swap files (irrespective of status)
    # is that the swapoff command will fail for any swap_file that is not enabled
    # (an edge case that could occur) meaning the entire task will fail unless
    # we loop over both swap_file *and* swap_enabled
    # But that has proven impractical so far (and I kept trying for a couple of hours...)
    # But I think we've found a middle ground by looping over only the enabled swaps
    # (those with return code 0)
    # Funnily enough, this task consistenly fails the first time, then works when
    # the playbook is rerun. Weird. Can we just ignore errors? Will that still work?
    - name: Disable any enabled swap file
      ansible.builtin.command: "swapoff {{ item.item.item }}"
      loop: "{{ swap_enabled.results }}"
      when: item.rc == 0
      ignore_errors: true

    - name: Delete all existing swap files
      ansible.builtin.file:
        path: "{{ item.item }}"
        state: absent
      when: item.stat.exists | bool
      loop: "{{ swap_file_check.results }}"

    - name: Remove all swap entries from fstab
      ansible.posix.mount:
        path: none
        fstype: swap
        state: absent

    # now go ahead and create *the swap file* with our desired filename
    - name: "Create {{ swap_file[0] }}"
      ansible.builtin.command: >
        dd if=/dev/zero of={{ swap_file[0] }} bs=1M count={{ swap_size }}

    - name: "Set proper permissions on {{ swap_file[0] }}"
      ansible.builtin.file:
        path: "{{ swap_file[0] }}"
        owner: root
        group: root
        mode: 0600

    - name: "Format the {{ swap_file[0] }} file"
      ansible.builtin.command: "mkswap {{ swap_file[0] }}"

    - name: "Add {{ swap_file[0] }} entry to fstab"
      ansible.posix.mount:
        path: none
        src: "{{ swap_file[0] }}"
        fstype: swap
        opts: sw
        passno: '0'
        dump: '0'
        state: present

    #  -f, --fixpgsz   reinitialize the swap space if necessary
    - name: Turn swap on
      ansible.builtin.command: swapon -f {{ swap_file[0] }}

    - name: "Set swappiness: {{ swappiness }}"
      ansible.posix.sysctl:
        name: vm.swappiness
        value: "{{ swappiness }}"
        state: present
  # END OF BLOCK
